const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");


router.post("/add", (request, response) => {
  const { book_id,book_title, publisher_name, author_name } = request.body;

  const connection = utils.openConnection();
  console.log(connection);
  const statement = `
        insert into Book
          ( book_id,
            book_title,
            publisher_name,
            author_name)
        values
          ( '${book_id}','${book_title}','${publisher_name}','${author_name}')
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});


router.get("/:book_title ", (request, response) => {
    const { book_title } = request.params;
  
    const connection = utils.openConnection();
  
    const statement = `
          select * from Book where
          book_title = '${book_title}'
        `;
    connection.query(statement, (error, result) => {
      connection.end();
      if (result.length > 0) {
        console.log(result.length);
        console.log(result);
        response.send(utils.createResult(error, result));
      } else {
        response.send("Book not found !");
      }
    });
  });

  router.put("/update/:book_id", (request, response) => {
    const { book_id } = request.params;
    const { publisher_name,author_name } = request.body;
  
    const statement = `
      update Book set
      publisher_name=${publisher_name},
      author_name=${author_name}
      where
      book_id = '${book_id}'
    `;
    const connection = utils.openConnection();
    connection.query(statement, (error, result) => {
      connection.end();
      console.log(statement);
  
      response.send(utils.createResult(error, result));
    });
  });
  


router.delete("/remove/:book_id", (request, response) => {
    const { book_id } = request.params;
    const statement = `
      delete from Book
      where
      book_id = '${book_id}'
      `;
    const connection = utils.openConnection();
    connection.query(statement, (error, result) => {
      connection.end();
      console.log(statement);
      response.send(utils.createResult(error, result));
    });
  });


module.exports = router;
